package config

const MaxScreenshotWidth = 4096
const MaxScreenshotHeight = 4096
const MinScreenshotWidth = 1
const MinScreenshotHeight = 0
const DefaultScreenshotWidth = 1280
const DefaultScreenshotHeight = 0
