package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"log"
	"net"
	"strconv"
	"time"

	"github.com/chromedp/cdproto/cdp"
	"github.com/chromedp/cdproto/emulation"
	"github.com/chromedp/cdproto/page"
	"github.com/chromedp/chromedp"
	"google.golang.org/grpc"

	pb "gitlab.ulb.tu-darmstadt.de/fid-bau/microservices/webshot/pkg/api/v1"
	"gitlab.ulb.tu-darmstadt.de/fid-bau/microservices/webshot/pkg/config"
)

func main() {
	var port int

	// flags declaration using flag package
	flag.IntVar(&port, "p", 3000, "Specify port to listen on")
	flag.Parse()

	listener, err := net.Listen("tcp", ":"+strconv.Itoa(port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	s := grpc.NewServer(grpc.MaxRecvMsgSize(1024*1024*500), grpc.MaxSendMsgSize(1024*1024*500))

	pb.RegisterWebshotServer(s, &server{})
	if err := s.Serve(listener); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}

type server struct {
	pb.UnimplementedWebshotServer
}

func (server) Webshoot(ctx context.Context, r *pb.WebshotRequest) (*pb.WebshotReply, error) {
	url := r.GetUrl()
	if url == "" {
		return nil, errors.New("missing query parameter 'url'")
	}

	// get screenshot dimensions from request params and validate
	width, height, err := validateDimensions(r)
	if err != nil {
		return nil, err
	}

	imageBuf, err := takeScreenshot(url, width, height, r.GetMobile(), r.GetEvaluateJavascript())
	if err != nil {
		return nil, err
	}
	return &pb.WebshotReply{
		Data: imageBuf,
	}, nil
}

func takeScreenshot(url string, width uint32, height uint32, mobile bool, evaluateJavascript string) (imageBuf []byte, err error) {
	log.Printf("taking screenshot of page %+v w=%+v, h=%+v, evaluateJavascript=%+v\n", url, width, height, evaluateJavascript)
	opts := append(chromedp.DefaultExecAllocatorOptions[:],
		// The default would be use-gl=desktop when there's a GPU we can
		// use, falling back to use-gl=swiftshader otherwise or when we
		// are running in headless mode. Swiftshader allows full WebGL
		// support with just a CPU.
		//
		// Unfortunately, many Linux distros like Arch and Alpine
		// package Chromium without Swiftshader, so we can't rely on the
		// defaults above. use-gl=egl works on any machine with a GPU,
		// even if we run Chrome in headless mode, which is OK for now.
		//
		// TODO(mvdan): remove all of this once these issues are fixed:
		//
		//    https://bugs.archlinux.org/task/64307
		//    https://gitlab.alpinelinux.org/alpine/aports/issues/10920
		chromedp.Flag("use-gl", "egl"),
	)

	// Start Chrome
	ctx, cancel := chromedp.NewExecAllocator(context.Background(), opts...)
	defer cancel()

	ctx, cancel = chromedp.NewContext(ctx)
	// ctx, cancel = chromedp.NewContext(ctx, chromedp.WithDebugf(log.Printf))
	defer cancel()

	ctx, cancel = context.WithTimeout(ctx, 120*time.Second)
	defer cancel()

	var actions []chromedp.Action
	actions = append(actions, emulation.SetDefaultBackgroundColorOverride().WithColor(&cdp.RGBA{R: 0, G: 0, B: 0, A: 0}))
	actions = append(actions, emulation.SetDeviceMetricsOverride(int64(width), int64(height), 1.0, mobile))
	actions = append(actions, navigateAndWaitForPageLoad(url))

	if len(evaluateJavascript) > 0 {
		actions = append(actions, chromedp.Evaluate(evaluateJavascript, nil))
	}
	if height > 0 {
		actions = append(actions, chromedp.ActionFunc(func(ctx context.Context) (err error) {
			imageBuf, err = page.CaptureScreenshot().WithFormat(page.CaptureScreenshotFormatPng).Do(ctx)
			return err
		}))
	} else {
		actions = append(actions, chromedp.FullScreenshot(&imageBuf, 100))
	}

	err = chromedp.Run(ctx, actions...)
	if err != nil {
		log.Println(err)
	}
	return imageBuf, err
}

func validateDimensions(r *pb.WebshotRequest) (uint32, uint32, error) {
	width := r.GetWidth()
	if width == 0 {
		width = config.DefaultScreenshotWidth
	}
	if width < config.MinScreenshotWidth || width > config.MaxScreenshotWidth {
		return 0, 0, fmt.Errorf("query parameter 'width' is not in the range [%d, %d]", config.MinScreenshotWidth, config.MaxScreenshotWidth)
	}
	height := r.GetHeight()
	if height == 0 {
		height = config.DefaultScreenshotHeight
	}
	if height < config.MinScreenshotHeight || height > config.MaxScreenshotHeight {
		return 0, 0, fmt.Errorf("query parameter 'height' is not in the range [%d, %d]", config.MinScreenshotHeight, config.MaxScreenshotHeight)
	}
	return width, height, nil
}

func navigateAndWaitForPageLoad(url string) chromedp.ActionFunc {
	return func(ctx context.Context) error {
		_, _, _, err := page.Navigate(url).Do(ctx)
		if err != nil {
			return err
		}

		cctx, cancel := context.WithCancel(ctx)

		idleTimeout := time.Second * 20

		timer := time.NewTimer(idleTimeout)
		defer timer.Stop()

		chromedp.ListenTarget(cctx, func(ev interface{}) {
			timer.Reset(idleTimeout)
		})
		select {
		case <-timer.C:
			cancel()
			return nil
		case <-ctx.Done():
			cancel()
			return ctx.Err()
		}
	}
}

/*
func navigateAndWaitForPageLoad(url string) chromedp.ActionFunc {
	return func(ctx context.Context) error {
		_, _, _, err := page.Navigate(url).Do(ctx)
		if err != nil {
			return err
		}

		ch := make(chan struct{})
		cctx, cancel := context.WithCancel(ctx)

		chromedp.ListenTarget(cctx, func(ev interface{}) {
			fmt.Printf("%v\n", ev)
			switch e := ev.(type) {
			case *page.EventLifecycleEvent:
				if e.Name == "networkIdle" {
					fmt.Println("--- network idle")
					cancel()
					close(ch)
				}
			}
		})
		select {
		case <-ch:
			return nil
		case <-ctx.Done():
			return ctx.Err()
		}
	}
}
*/
